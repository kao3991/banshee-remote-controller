<?php
class Banshee {
	protected $pid;
	public function prepare() {
		$this -> loadPid();
	}
	public function loadPid() {
		$command = 'ps ax | grep Banshee.exe | grep -v grep';
		$output = array();
		exec($command, $output);
		$tmp = explode(' ', $output[0]);
		$this -> pid = $tmp[0];
	}

	public function preCommand() {
		return 'export $(strings /proc/'.$this -> pid.'/environ| grep DBUS_SESSION | tail -1)';
	}
	public function runCommand($command, &$output) {
		exec($this -> preCommand().' && '.$command, $output);
	}
	public function playNext() {
		$out = [];
		$this -> runCommand('banshee --next', $out);
		var_dump($out);
	}
	public function playPrev() {
		$out = [];
		$this -> runCommand('banshee --previous', $out);
		var_dump($out);
	}
	public function getState() {
		$out = [];
		$state = [];
		$this -> runCommand('banshee --query-current-state --query-artist --query-title --query-album --query-duration --query-volume', $out);
		foreach ($out as $line) {
			$tmp = explode(':', $line, 2);
			$state[$tmp[0]] = trim($tmp[1]);
		}
		return $state;	
	}
	public function pause() {
		$this -> runCommand('banshee --pause');	
	}
	public function play() {
		$this -> runCommand('banshee --play');	
	}
	public function setVolume($volume) {
		$this -> runCommand('banshee --set-volume='.$volume);	
	}
}
